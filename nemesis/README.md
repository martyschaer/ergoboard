# Nemesis
Nemesis will be the second iteration in the DIY keyboard journey.
Hypnos works nicely, but I have hard time adapting to it.
Thus Nemesis will be designed to more closely resemble the Preonic
I'm currently using, while also being more ergonimic.

## Design Ideas
- two halves of 30 keys (5 rows x 6 columns)
- possibly ommit the number row, so only 4rx6c
- whole keyboard possibly wireless
- halves wired together using a type-c wire
    * wire through the row/col wires, not a 2nd controller on the sub-half 
- build it using a matrix with diodes
    * http://blog.komar.be/how-to-make-a-keyboard-the-matrix/
- possibly make use of an SMD assembly service
- designed to fit a 3d-printed case
- tenting
